﻿using Abp.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.DataAccess.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class EfRepository<T> : 
        IRepository<T>
        where T: BaseEntity
    {
        private readonly SqlLiteDbContext _dbContext;

        public EfRepository(SqlLiteDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task CreateAsync(T entity)
        {
            _dbContext.Set<T>().Add(entity);
            await SaveChanges();
        }

        public async Task<IEnumerable<T>>  GetAllAsync(params Expression<Func<T, object>>[] includes)
        {
            IQueryable<T> query = _dbContext.Set<T>();

            if (includes.Count()>0)
            {
                query = Include(query, includes);
            }
            return  await query.ToListAsync();

        }

        private IQueryable<T> Include (IQueryable<T> query, params Expression<Func<T, object>>[] includes)
        {
            foreach (var include in includes)
            {
                query = query.Include(include);
            }
            return query;
        }
        public async Task<T> GetByIdAsync(Guid id, params Expression<Func<T, object>>[] includes)
        {
            var query = _dbContext.Set<T>().Where(x => x.Id == id);
            
            if (includes.Count() > 0)
            {
                query = Include(query, includes);
            }

            return  await query.FirstOrDefaultAsync();
        } 
       
        public async Task UpdateAsync(T entity)
        {
            var dbSet = _dbContext.Set<T>();
            var isFound = dbSet.Any(x => x.Id == entity.Id);

            if (isFound)
                _dbContext.Set<T>().Update(entity);

            await SaveChanges();
        }

        public async Task DeleteAsync(Guid id)
        {
            var dbSet = _dbContext.Set<T>();
            var entity = await dbSet.FirstOrDefaultAsync(x => x.Id == id);

            if (entity != null)
                dbSet.Remove(entity);
            else
                throw new EntityNotFoundException();

            await SaveChanges();
        }

        private async Task SaveChanges()
           => await _dbContext.SaveChangesAsync();

    }
}
