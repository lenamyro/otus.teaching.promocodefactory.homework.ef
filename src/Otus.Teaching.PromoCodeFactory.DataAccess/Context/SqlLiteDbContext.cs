﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Context
{
    public class SqlLiteDbContext: DbContext
    {
        public SqlLiteDbContext(DbContextOptions<SqlLiteDbContext> options)
            : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder options)
            => options.UseSqlite("Data Source=promocodes.db");

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Customer>().ToTable("customers");

            modelBuilder.Entity<Employee>()
                        .HasOne(r => r.Role)
                        .WithMany(e => e.Employees)
                        .OnDelete(DeleteBehavior.SetNull);
        }

        public async void GenerateDbWithFakeData()
        {
            var employees = FakeDataFactory.Employees;
            var roles = FakeDataFactory.Roles;
            var preferences = FakeDataFactory.Preferences;
            var customers = FakeDataFactory.Customers;

            var preferencesEntity = Set<Preference>();

            foreach (var customer in customers)
            {
                Set<Customer>().Add(customer);
            }

            foreach (var employee in employees)
            {
                Set<Employee>().Add(employee);
            }
            SaveChanges();
        }
    }
}
