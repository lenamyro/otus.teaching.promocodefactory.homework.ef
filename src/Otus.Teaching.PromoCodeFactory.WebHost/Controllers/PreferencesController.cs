﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Clients API
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PreferencesController
        : ControllerBase
    {
        private readonly IRepository<Preference> _repository;
        public PreferencesController(IRepository<Preference> repository)
        {
            _repository = repository;
        }

        /// <summary>
        /// Get list all preferences
        /// </summary>
        /// <returns>List<PreferenceResponse></returns>
        [HttpGet]
        public async Task<ActionResult<PreferenceResponse>> GetPreferencesAsync()
        {
            var preferences = _repository.GetAllAsync().Result;
           
            var list = preferences.Select(x =>
               new PreferenceResponse()
               {
                   Id = x.Id.ToString(),
                   Name = x.Name
                  
               }).ToList();

            return Ok(list);
        }
    }
}