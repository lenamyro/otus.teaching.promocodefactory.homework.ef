﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Clients API
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {
        private readonly IRepository<Customer> _repository;
        public CustomersController(IRepository<Customer> repository)
        {
            _repository = repository;
        }

        /// <summary>
        /// Get list all clients
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<CustomerShortResponse>> GetCustomersAsync()
        {
            var customers = await  _repository.GetAllAsync();
           
            var list = customers.Select(x =>
               new CustomerShortResponse()
               {
                   Id = x.Id,
                   Email = x.Email,
                   FirstName = x.FirstName,
                   LastName = x.LastName
               }).ToList();

            return Ok(list);
        }
        
        /// <summary>
        /// Get customer by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            var customer = _repository.GetByIdAsync(id, d=>d.Preferences).Result;

            var response = new CustomerResponse()
            {
                Id = customer.Id,
                FirstName = customer.FirstName,
                LastName = customer.LastName,
                Email = customer.Email,
                Preferences = customer.Preferences.Select(x => new PreferenceResponse()
                    {
                        Name = x.Name
                    }).ToList()
            };

            return Ok(response);
        }
        
        /// <summary>
        /// create a new customer with his preferences
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            _repository.CreateAsync(request.ToCustomer());
            return Ok();
        }
        
        /// <summary>
        /// edit customer by id with all his preferences
        /// </summary>
        /// <param name="id"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
             _repository.UpdateAsync(request.ToCustomer()).GetAwaiter();
            return Ok();
        }

        /// <summary>
        /// delete client by id with all connected promocodes
        /// </summary>
        /// <param name="id"></param>
        /// <response code="201">Customer was created succesfully</response>
        /// <response code="400">Bad request</response>
        [HttpDelete]
        public async Task<IActionResult> DeleteCustomer(Guid id)
        {
            if (id ==  Guid.Empty)
                return StatusCode(400);

            await _repository.DeleteAsync(id);
            return Ok();
        }
    }
}